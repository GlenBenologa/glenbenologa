import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://service.posterguard.com/Welcome')

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Welcome/a_Account Administrators'))

WebUI.setText(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Log On/input_Email_userNameOrEmail'), 
    'miaoglenda@yahoo.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Log On/input_Password_password'), 
    'SVMgu6XhQ9PCb60VBI/JSA==')

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Log On/button_Sign In'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/div_Home       Quick Links'))

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/a_RESOURCES'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard/div_Posting Updates'))

WebUI.mouseOver(findTestObject('Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/a_RESOURCES_hover'))

WebUI.mouseOverOffset(findTestObject('Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/a_RESOURCES_hover'), 
    3, 3)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard/a_Poster Audit'))

WebUI.delay(5)

WebUI.navigateToUrl('https://service.posterguard.com/Resources')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard/div_Posting Updates'))

WebUI.mouseOver(findTestObject('Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/a_RESOURCES_hover'))

WebUI.mouseOverOffset(findTestObject('Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/a_RESOURCES_hover'), 
    3, 3)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard/a_Labor Law Blog'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Labor Law Blog/a_more'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Employers Should/button_Submit Comment'))

WebUI.delay(3)

WebUI.navigateToUrl('https://service.posterguard.com/Resources')

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard/div_Posting Updates'))

WebUI.mouseOver(findTestObject('Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/a_RESOURCES_hover'))

WebUI.mouseOverOffset(findTestObject('Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/a_RESOURCES_hover'), 
    3, 3)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Employers Should/a_Labor Law Blog'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Labor Law Blog/a_more'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Employers Should/textarea_Comment_Comments.Comm'), 
    'This is test comment...')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Employers Should/button_Submit Comment'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard/div_Posting Updates'))

WebUI.mouseOver(findTestObject('Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/a_RESOURCES_hover'))

WebUI.mouseOverOffset(findTestObject('Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/a_RESOURCES_hover'), 
    3, 3)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Employers Should/a_Minimum Wage Monitor'))

WebUI.delay(5)

WebUI.navigateToUrl('https://service.posterguard.com/Resources')

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard/div_Posting Updates'))

WebUI.mouseOver(findTestObject('Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/a_RESOURCES_hover'))

WebUI.mouseOverOffset(findTestObject('Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/a_RESOURCES_hover'), 
    3, 3)

WebUI.click(findTestObject('Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Dashboard/a_Site Tutorials_hover'))

WebUI.delay(10)

WebUI.click(findTestObject('Resources/Resources_SubCategories_FINAL/Page_PosterGuard - Tutorials/div_.afill000opacity0.65.bfill_hover'))

WebUI.delay(30)

WebUI.click(findTestObject('Object Repository/Resources/Resources_SubCategories_FINAL/Page_PosterGuard/a_Sign Out'))

WebUI.closeBrowser()

