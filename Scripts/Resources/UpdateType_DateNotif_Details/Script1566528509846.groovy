import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://service.posterguard.com/Welcome')

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard - Welcome/a_Account Administrators'))

WebUI.setText(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard - Log On/input_Email_userNameOrEmail'), 
    'miaoglenda@yahoo.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard - Log On/input_Password_password'), 
    'SVMgu6XhQ9PCb60VBI/JSA==')

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard - Log On/button_Sign In'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard - Dashboard/div_Home       Quick Links'))

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard - Dashboard/a_RESOURCES'))

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/button_Non-Mandatory'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/button_Mandatory'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/button_Clear All Selections'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/input_Notification Date_dateRa'), 
    '08-19-2019')

WebUI.setText(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/input_Sa_daterangepicker_end'), 
    '08/20/2019')

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/button_Apply'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/a_Clear Date'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/div_Fed  State  Province'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/p_Select Option'))

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/div_VI'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/div_Update Information'))

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/div_Update Details'))

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/button_Clear All Selections'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/UpdateType_DateNotif_Details/Page_PosterGuard/a_Sign Out'))

WebUI.delay(3)

WebUI.closeBrowser()

