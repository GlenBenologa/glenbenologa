import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://service.posterguard.com/Welcome')

WebUI.click(findTestObject('Object Repository/Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard - Welcome/a_Account Administrators'))

WebUI.setText(findTestObject('Object Repository/Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard - Log On/input_Email_userNameOrEmail'), 
    'miaoglenda@yahoo.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard - Log On/input_Password_password'), 
    'SVMgu6XhQ9PCb60VBI/JSA==')

WebUI.click(findTestObject('Object Repository/Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard - Log On/button_Sign In'))

WebUI.click(findTestObject('Object Repository/Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard - Dashboard/div_Home       Quick Links'))

WebUI.click(findTestObject('Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard - Dashboard/a_RESOURCES_Hover'))

WebUI.click(findTestObject('Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard - Dashboard/a_Poster Audit_droplist'))

WebUI.click(findTestObject('Object Repository/Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard - Dashboard/div_Home       Quick Links'))

WebUI.click(findTestObject('Object Repository/Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard - Dashboard/span_RESOURCES_fa fa-chevron-d'))

WebUI.click(findTestObject('Object Repository/Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard/div_Posting Updates'))

WebUI.click(findTestObject('Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard - Dashboard/a_Minimum Wage Monitor_droplist'))

WebUI.click(findTestObject('Object Repository/Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard/div_Posting Updates'))

WebUI.click(findTestObject('Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard - Dashboard/a_Site Tutorials_droplist'))

WebUI.click(findTestObject('Object Repository/Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard - Tutorials/img'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard/div_Posting Updates'))

WebUI.click(findTestObject('Object Repository/Resources/PosterUpdate_MinWage_SiteTutorial/Page_PosterGuard/a_Sign Out'))

