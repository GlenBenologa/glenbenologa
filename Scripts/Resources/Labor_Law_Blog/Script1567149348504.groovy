import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://service.posterguard.com/Welcome')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Welcome/a_Account Administrators'))

WebUI.setText(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Log On/input_Email_userNameOrEmail'), 
    'miaoglenda@yahoo.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Log On/input_Password_password'), 
    'SVMgu6XhQ9PCb60VBI/JSA==')

WebUI.click(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Log On/button_Sign In'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Dashboard/div_Home       Quick Links'))

WebUI.mouseOver(findTestObject('Resources/Labor Law Blog/Page_PosterGuard - Dashboard/a_RESOURCES_Hover'))

WebUI.waitForElementVisible(findTestObject('Resources/Labor Law Blog/Page_PosterGuard - Dashboard/a_Labor Law Blog'), 3)

WebUI.click(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Dashboard/a_Labor Law Blog'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Labor Law Blog/a_more'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Employers Should/button_Submit Comment'))

WebUI.delay(3)

WebUI.navigateToUrl('https://service.posterguard.com')

WebUI.click(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Dashboard/div_Home       Quick Links'))

WebUI.mouseOver(findTestObject('Resources/Labor Law Blog/Page_PosterGuard - Dashboard/a_RESOURCES_Hover'))

WebUI.waitForElementVisible(findTestObject('Resources/Labor Law Blog/Page_PosterGuard - Dashboard/a_Labor Law Blog'), 5)

WebUI.click(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Dashboard/a_Labor Law Blog'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Labor Law Blog/a_more'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Employers Should/textarea_Comment_Comments.Comm'), 
    'this test comment...')

WebUI.click(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Employers Should/button_Submit Comment'))

WebUI.click(findTestObject('Object Repository/Resources/Labor Law Blog/Page_PosterGuard - Employers Should/a_Sign Out'))

WebUI.delay(3)

WebUI.closeBrowser()

