import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://service.posterguard.com/Welcome')

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard - Welcome/a_Account Administrators'))

WebUI.setText(findTestObject('Object Repository/Resources/Page_PosterGuard - Log On/input_Email_userNameOrEmail'), 'miaoglenda@yahoo.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Resources/Page_PosterGuard - Log On/input_Password_password'), 
    'SVMgu6XhQ9PCb60VBI/JSA==')

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard - Log On/button_Sign In'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard - Dashboard/div_Home       Quick Links'))

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard - Dashboard/a_RESOURCES'))

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/p_Select Option'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_Federal'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_DE'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_Federal'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_FL'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_LA'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_MA'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_FL'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_DE'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_MN'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_MA'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_NJ'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_WI'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_LA'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_PE'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_NB'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_SK'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_AB'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_NS'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_TN'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_PA'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_NE'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_NV'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_VT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_WA'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_OR'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_NL'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_ON'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_WV'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_WY'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_VI'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_TX'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_VA'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_UT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_GA'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_GU'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_AK'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/div_HI'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/button_Clear All Selections'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Resources/Page_PosterGuard/a_Sign Out'))

WebUI.delay(3)

WebUI.closeBrowser()

