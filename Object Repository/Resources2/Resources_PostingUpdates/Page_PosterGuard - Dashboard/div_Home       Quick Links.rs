<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Home       Quick Links</name>
   <tag></tag>
   <elementGuidId>7bdd0f0a-8233-4c88-b7b1-2ec04f2c36b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Content']/div/article/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>hm_wrapper</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>


Home



          

Quick Links





             
View My Services


           
View My Shipments


         
View My Invoices


         
View Posting Updates


     
Access Employee Handouts







         

Recent Posting Updates
View all



Aug 23

 Nevada Annual Daily Overtime Bulletin  


Mandatory Update


The Nevada Annual Daily Overtime Bulletin has been updated to reflect a clarification on when employers must pay overtime rates to...
Read More



Aug 23

 CANADA: Ontario Occupational Health and Safety  


Mandatory Update


The Ontario Occupational Health and Safety poster has been updated to reflect that the Chief Prevention Officer sets out the minimum...
Read More



Aug 21

 Virginia OSHA  


Mandatory Update


The Virginia OSHA poster has been updated to reflect new mandatory penalty amounts for each serious violation, non-serious violation,...
Read More



Aug 21

 Washington Domestic Violence Resources  


Mandatory Update


The Washington Domestic Violence Resources poster has been released by the Washington Employment Security...  
Read More







         

Recent Blog Posts
View all



Aug 19

 Minneapolis, Minnesota Passes Wage Theft Ordinance 


The Minneapolis City Council recently passed the Wage Theft Prevention Ordinance.  The new ordinance takes effect on January 1, 2020,...
Read More



Aug 12

 New York Governor Cuomo Signs Legislation Prohibiting... 

Governor Andrew M. Cuomo recently signed legislation (S.04037 /A.4204) which prohibits employment discrimination based on religious...
Read More






           

Upcoming Webinars




Sept 12

 Special Posting Requirements for Businesses with Remote Workers     


Thursday, September 12, 2019
11 a.m. PST \ 1 p.m. CST \ 2 p.m. EST

Register Now





</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Content&quot;)/div[@class=&quot;zone zone-content&quot;]/article[@class=&quot;page content-item&quot;]/div[@class=&quot;hm_wrapper&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='Content']/div/article/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tuesday, February 09, 2016 8:41:00 AM'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//article/div</value>
   </webElementXpaths>
</WebElementEntity>
