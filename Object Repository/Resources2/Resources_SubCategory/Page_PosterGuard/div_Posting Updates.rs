<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Posting Updates</name>
   <tag></tag>
   <elementGuidId>076c3857-f49d-408d-9512-1393ba894727</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ContentArea']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                

                        

    
        Posting Updates
    

    
        
        




    
        Filter Updates
    
    
        
            Clear All Selections
        
    




    
        
                
                    Fed/State/Province
                
                
                    
                        
                            
                                
                                     Select Option
                                
                            
                        
                    
                    
                        
                            

                                
                                FedFederalStates (USA)AKALARAZCACOCTDCDEFLGAGUHIIAIDILINKSKYLAMAMDMEMIMNMOMSMTNCNDNENHNJNMNVNYOHOKORPAPRRISCSDTNTXUTVAVIVTWAWIWVWYProvinces (Canada)ABNBNLNSONPESK
                                
                            
                        
                    
                
                
                    Update Type
                
                

                        
                            
                                Non-Mandatory
                            
                        
                        
                            
                                Mandatory
                            
                        

                
    
        Notification Date
    
    
        

            
                
                
                
                    Clear Date 
                
            
        
    
    



    $(document).ready(function () {
        initTimePicker('/Resources/List');
    });


    

    

        
        
            
        

        
        

    
   

    

    
            1070 Results
 
    

    

            Results Per Page 25
50
75
100


    



    

    
        
            
                
                    
                        
                            
                                Fed / State / Province

                            
                        
                        
                            
                                Update Information
                            
                        
                        
                            
                                Update Details
                            
                        
                        
                    
                
                

                    
                                                        

                                    

                                        
.pg-resources-region-1{background: url('/Modules/PG.Resources/Content/Images/NV.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}NV
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            
                                                                    NEW!

                                                            Nevada Annual Daily Overtime Bulletin
                                                        
                                                    
                                                        Notification Date: 08-23-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Nevada Annual Daily Overtime Bulletin has been updated to reflect a clarification on when employers must pay overtime rates to employees. This posting appears on the Nevada Combination Poster.  Revision Date: 08/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-2{background: url('/Modules/PG.Resources/Content/Images/ON.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}ON
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            
                                                                    NEW!

                                                            Ontario Occupational Health and Safety
                                                        
                                                    
                                                        Notification Date: 08-23-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Ontario Occupational Health and Safety poster has been updated to reflect that the Chief Prevention Officer sets out the minimum criteria that has to be met in order for a Joint Health and Safety Committee certification training program to be approved.  Revision Date: 08/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-3{background: url('/Modules/PG.Resources/Content/Images/VA.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}VA
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            
                                                                    NEW!

                                                            Virginia OSHA
                                                        
                                                    
                                                        Notification Date: 08-21-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Virginia OSHA poster has been updated to reflect new mandatory penalty amounts for each serious violation, non-serious violation, and daily penalty for failing to correct a violation. The penalty for willful violations was also increased. This posting appears on the Virginia Combination Poster.  Revision Date: 08/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-4{background: url('/Modules/PG.Resources/Content/Images/WA.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}WA
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            
                                                                    NEW!

                                                            Washington Domestic Violence Resources
                                                        
                                                    
                                                        Notification Date: 08-21-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Washington Domestic Violence Resources poster has been released by the Washington Employment Security Department.  A new law requires employers to display a poster regarding domestic violence and that also provides information on community resources regarding domestic violence.  This posting appears on the Washington Combination Poster.  Revision Date: 08/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-5{background: url('/Modules/PG.Resources/Content/Images/AR.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}AR
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            
                                                                    NEW!

                                                            Arkansas Minimum Wage
                                                        
                                                    
                                                        Notification Date: 08-19-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Arkansas Minimum Wage poster has been updated to reflect that the Secretary of the Department of Labor and Licensing now enforces Arkansas’ minimum wage laws. The poster was also updated with new contact information for the Department of Labor and Licensing.  This posting appears on the Arkansas Combination Poster.  Revision Date: 08/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-6{background: url('/Modules/PG.Resources/Content/Images/MO.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}MO
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            
                                                                    NEW!

                                                            Missouri Workers Compensation
                                                        
                                                    
                                                        Notification Date: 08-19-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Missouri Workers Compensation poster has been updated to clarify that employees must provide written notice within 30 days of an accident or within 30 days after the diagnosis of any occupational disease or repetitive trauma.  This posting appears on the Missouri Combination Poster.  Revision Date: 08/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-7{background: url('/Modules/PG.Resources/Content/Images/NB.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}NB
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            
                                                                    NEW!

                                                            New Brunswick Occupational Health and Safety
                                                        
                                                    
                                                        Notification Date: 08-16-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The New Brunswick Occupational Health and Safety poster has been updated to reflect that the Workplace Health, Safety and Compensation Commission must review the Act and recommend any changes every five years. The poster was also updated with information about approved organizations providing educational programs. Revision Date: 08/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-8{background: url('/Modules/PG.Resources/Content/Images/AZ.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}AZ
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            
                                                                    NEW!

                                                            Flagstaff, AZ Employment Discrimination
                                                        
                                                    
                                                        Notification Date: 08-08-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Flagstaff, AZ Employment Discrimination poster has been updated to clarify how employees may file discrimination complaints. Revision Date: 08/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-9{background: url('/Modules/PG.Resources/Content/Images/CA.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}CA
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            
                                                                    NEW!

                                                            Emeryville, CA Minimum Wage and Paid Sick Leave
                                                        
                                                    
                                                        Notification Date: 08-07-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Emeryville, CA Minimum Wage and Paid Sick Leave poster has been updated to reflect an increase to the city minimum wage.  For all employers, the minimum wage will increase to $16.30 per hour effective July 1, 2019.  For small, independent restaurants,  the minimum wage will increase to $16.30 per hour effective July 10, 2019.  Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-10{background: url('/Modules/PG.Resources/Content/Images/NY.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}NY
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            COUNTY CONTRACTOR: Nassau County, NY Living Wage
                                                        
                                                    
                                                        Notification Date: 08-02-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Nassau County, NY Living Wage poster for county contractors has been updated to reflect an increase in the living wage.  The living wage will increase from $16.41 per hour to $17.26 per hour for employees without health benefits and from $14.27 per hour to $14.51 per hour for employees with health benefits. Revision Date: 08/01

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-11{background: url('/Modules/PG.Resources/Content/Images/MA.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}MA
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            Massachusetts No Smoking
                                                        
                                                    
                                                        Notification Date: 08-01-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Massachusetts No Smoking poster has been updated to reflect that vaping is prohibited in places of employment. The law requires places of employment to be smoke-free and vape-free. Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-12{background: url('/Modules/PG.Resources/Content/Images/VT.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}VT
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            Vermont Family Leave
                                                        
                                                    
                                                        Notification Date: 08-01-2019
                                                        Non-Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Vermont Family Leave poster has been updated to reflect a phone number and web address change when obtaining copies of the poster from the Vermont Department of Labor.

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-13{background: url('/Modules/PG.Resources/Content/Images/WA.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}WA
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            Washington Unemployment Insurance
                                                        
                                                    
                                                        Notification Date: 08-01-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Washington Unemployment Insurance poster has been updated to reflect additional requirements for filing an unemployment insurance claim and how to file online and by phone. This posting appears on the Washington Combination Poster. Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-14{background: url('/Modules/PG.Resources/Content/Images/SD.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}SD
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            South Dakota Unemployment Insurance
                                                        
                                                    
                                                        Notification Date: 07-31-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The South Dakota Unemployment Insurance poster has been updated to reflect the renaming of the Unemployment Insurance Program to the Reemployment Assistance Program. The web address for filing claims was also updated. This posting appears on the South Dakota Combination Poster.  Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-15{background: url('/Modules/PG.Resources/Content/Images/NJ.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}NJ
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            RESTAURANT: New Jersey Food Allergy Awareness
                                                        
                                                    
                                                        Notification Date: 07-30-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The State of New Jersey has released the Food Allergy Awareness for Food Workers poster.  Restaurants are required to display this poster informing employees of common food allergens. Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-16{background: url('/Modules/PG.Resources/Content/Images/NM.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}NM
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            New Mexico No Smoking
                                                        
                                                    
                                                        Notification Date: 07-26-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The New Mexico No Smoking poster has been updated to reflect that vaping is prohibited in places of employment. The new law requires places of employment to be smoke-free and vape-free. Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-17{background: url('/Modules/PG.Resources/Content/Images/NV.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}NV
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            Nevada Employee Sick or Sustained Injury
                                                        
                                                    
                                                        Notification Date: 07-26-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Nevada Employee Sick or Sustained Injury poster was released by the Nevada Office of the Labor Commissioner. The new poster informs employees that an employer cannot require employees to be present at the workplace in order to notify the employer that they cannot work due to an illness or injury unrelated to work. This posting appears on the Nevada Combination Poster.  Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-18{background: url('/Modules/PG.Resources/Content/Images/NV.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}NV
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            Nevada Minimum Wage
                                                        
                                                    
                                                        Notification Date: 07-26-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Nevada Minimum Wage poster has been updated to clarify that employee deductions must be in writing and when rest periods must be given to employees. The poster was also updated with information about the new paid leave law. This posting appears on the Nevada Combination Poster. Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-19{background: url('/Modules/PG.Resources/Content/Images/NV.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}NV
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            Nevada Minimum Wage Bulletin
                                                        
                                                    
                                                        Notification Date: 07-26-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Nevada Minimum Wage Bulletin has been updated to clarify when employers can pay a lower minimum wage rate. The poster was also updated with the new minimum wage rates for July 1, 2020 through July 1, 2024.  This posting appears on the Nevada Combination Poster. Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-20{background: url('/Modules/PG.Resources/Content/Images/NV.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}NV
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            Nevada Paid Leave
                                                        
                                                    
                                                        Notification Date: 07-26-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Nevada Paid Leave poster has been released by the Nevada Office of the Labor Commissioner. The Paid Leave Act requires employers with 50 or more employees to provide employees 0.01923 hours of paid leave for each hour worked, up to a maximum of 40 hours of paid leave per benefit year. The new law is effective January 1, 2020. This posting appears on the Nevada Combination Poster.  Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-21{background: url('/Modules/PG.Resources/Content/Images/FL.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}FL
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            HEALTHCARE:  Florida Human Trafficking
                                                        
                                                    
                                                        Notification Date: 07-24-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Florida Department of Health has released a new poster regarding human trafficking.  The new poster informs employees about the illegality of human trafficking and how to report it.  It must be posted by certain businesses including hotels and healthcare professionals. Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-22{background: url('/Modules/PG.Resources/Content/Images/CO.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}CO
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            Colorado Fair Employment
                                                        
                                                    
                                                        Notification Date: 07-22-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Colorado Fair Employment poster has been updated to reflect new protected categories. Employers are prohibited  from discriminating against employees who inquire about, disclose, or discuss their wages. The poster was also updated with information about reasonable accommodations for employees with disabilities. This posting appears on the Colorado Combination Poster. Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-23{background: url('/Modules/PG.Resources/Content/Images/ND.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}ND
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            HEALTHCARE: North Dakota Radiation Notice to Employees
                                                        
                                                    
                                                        Notification Date: 07-22-2019
                                                        Non-Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The North Dakota Radiation Notice to Employees poster has been changed to reflect a change to the name of the department and a change of their website address. Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-24{background: url('/Modules/PG.Resources/Content/Images/IN.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}IN
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            Indiana OSHA
                                                        
                                                    
                                                        Notification Date: 07-17-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The Indiana OSHA poster has been updated to reflect an amendment made to the Indiana Occupational Safety and Health Act. The law was amended to increase the maximum penalty to $132,598 for each employee fatality resulting from an employer violating safety regulations. This posting appears on the Indiana Combination Poster. Revision Date: 07/19

                                    
                                    
                                
                                

                                    

                                        
.pg-resources-region-25{background: url('/Modules/PG.Resources/Content/Images/TX.png');background-repeat: no-repeat;padding: 0;vertical-align: middle !important;background-size: 50%;background-position: center;line-height: 2.1;}TX
                                    
                                    
                                        
                                            

                                                
                                                    
                                                        
                                                            

                                                            Dallas, TX Paid Sick Leave
                                                        
                                                    
                                                        Notification Date: 07-16-2019
                                                        Mandatory Update

                                            
                                        
                                    
                                    

                                        
                                            The City of Dallas, TX has released the new Paid Sick Leave poster.  The City of Dallas Paid Sick Leave Ordinance requires employers to provide employees with paid sick leave to care for themselves or a family member.  The new law requires employers to post this notice informing employees of their rights to paid sick leave in both English and Spanish. Revision Date: 07/19

                                    
                                    
                                
                    
                
            
        

        
            
                
&lt;&lt;&lt;12345...>>>                
            



    

    
        
    

    
        $(document).ready(initResources('/Resources/List'));
        $(document).ready(initFilters('/PG.Resources/Resources/FilterableOptions'));
    

                
            
        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentArea&quot;)/div[@class=&quot;row&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='ContentArea']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Site Tutorials'])[1]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Minimum Wage Monitor'])[1]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//body/div[3]/div/div</value>
   </webElementXpaths>
</WebElementEntity>
