<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>body_iframe srcwww.googletagma</name>
   <tag></tag>
   <elementGuidId>05dc152b-e2bd-431a-a233-483635b773bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>body</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>







    &lt;iframe src=&quot;//www.googletagmanager.com/ns.html?id=GTM-5XBX3S&quot;
            height=&quot;0&quot; width=&quot;0&quot; style=&quot;display:none;visibility:hidden&quot;>&lt;/iframe>


    (function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
                new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&amp;l=' + l : ''; j.async = true; j.src =
            '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-5XBX3S');



/*&lt;![CDATA[*/window.zEmbed || function (e, t) { var n, o, d, i, s, a = [], r = document.createElement(&quot;iframe&quot;); window.zEmbed = function () { a.push(arguments) }, window.zE = window.zE || window.zEmbed, r.src = &quot;javascript:false&quot;, r.title = &quot;&quot;, r.role = &quot;presentation&quot;, (r.frameElement || r).style.cssText = &quot;display: none&quot;, d = document.getElementsByTagName(&quot;script&quot;), d = d[d.length - 1], d.parentNode.insertBefore(r, d), i = r.contentWindow, s = i.document; try { o = s } catch (c) { n = document.domain, r.src = 'javascript:var d=document.open();d.domain=&quot;' + n + '&quot;;void(0);', o = s } o.open()._l = function () { var o = this.createElement(&quot;script&quot;); n &amp;&amp; (this.domain = n), o.id = &quot;js-iframe-async&quot;, o.src = e, this.t = +new Date, this.zendeskHost = t, this.zEQueue = a, this.body.appendChild(o) }, o.write('&lt;body onload=&quot;document._l();&quot;>'), o.close() }(&quot;https://assets.zendesk.com/embeddable_framework/main.js&quot;, &quot;posterguard.zendesk.com&quot;);/*]]>*/



    if (typeof jQuery == 'undefined') { document.write('&lt;script type=&quot;text/javascript&quot; src=&quot;/Themes/PG/js/jquery.min.js&quot;>&lt;' + '/script>'); }
    var style_mobile_path = '/Themes/PG/styles/css/';


    $('head').append('&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0&quot;>');









    
    
        $(document).ready(ajaxLoadPartialView('/NavUserMenu', &quot;#TopNavUserMenu&quot;, &quot;&quot;));
        $(document).ready(ajaxLoadPartialView('/CustomerSearch/Index', &quot;#CSRCustomerSearch&quot;, &quot;&quot;));
        $(document).ready(ajaxLoadPartialView('/HeaderMessage', &quot;#HeaderMessage&quot;, &quot;&quot;));
    

    
    
    $(document).ready(initCustomerSearch('/CustomerSearch/Index', '/CustomerSearch/GetResults', '/Services'));



    
        
            
                
            
        

        
        
            
        

        
            
                
                    
                        
                        Toggle Dropdown
                    
                    
                        
                        
                            Select Category
                            Account #
                            Account Phone #
                            
                            PG Service ID
                            Parent ID
                            
                            Delivery Email
                            Delivery Company
                            Delivery Address
                        
                        
                    
                

                
                    

                
            
        
    

    
    


    
    

    
    

    
    
    
        
            
                    
                        
Welcome, Glenda QA                        
                    
                    
                         My Account
                    
                    
                         My Messages
                    
                    
                         Help
                    

                
                    
                        
                            Sign Out
                        

                    
                
            
        
        

    
    

    
    
        
            
                
                    
                
            
        

        

                
                    $(document).ready(ajaxLoadNavigation('/TopNavigation', &quot;#TopNavigation&quot;, true));
                
                
                    
                        HOMESERVICESSHIPMENTSINVOICESRESOURCES Posting UpdatesPoster AuditLabor Law BlogMinimum Wage MonitorSite Tutorials
                    
                

        
    
    
    


    
    







    
    
        
    
    

    
        
            
                

                        

    
        

Employers Should Continue to Use the Current Form I-9

    
        Tags:
Form I-9, Immigration    

            
                Wednesday, August 28, 2019 1:50:17 PM
            
    
    Shanna Wall, Compliance Attorney
USCIS has issued a notice that employers should continue to use the current Form I-9 beyond the expiration date of August 31, 2019. Employers should continue to use this form until the new version becomes available.  We will continue to provide updates when available.

No Comments





    


    
            
                

 Hi, Glenda! 
    
        
            Comment
            
        
    
    
    


    

            
    
    
            


    Submit Comment


                
            
        

    

    




    
        
            
                
                
            
        
 
        
            
                100% Compliance Guarantee
                Privacy Policy
                Terms &amp; Conditions for Poster Guard
            
        

        
            
                Terms &amp; Conditions for Employee Handouts
                Website Terms of Use
                Help
                Contact Us 
                Site Map
            
        
    
    
        Server F5P1
    
























/html[@class=&quot;dyn detail-blog-post contents&quot;]/body[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;dyn detail-blog-post contents&quot;]/body[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//body</value>
   </webElementXpaths>
</WebElementEntity>
